//
//  ViewController.swift
//  Reconocimiento de Voz
//
//  Created by JUAN on 5/01/18.
//  Copyright © 2018 net.juanfrancisco.blog. All rights reserved. (Todo es libre , viva liberta)
//https://code.tutsplus.com/es/tutorials/using-the-speech-recognition-api-in-ios-10--cms-28032

import UIKit
import Speech
import AVFoundation
class ViewController: UIViewController,AVAudioRecorderDelegate {

    @IBOutlet weak var richText: UITextView!

    var audioRecondingSession : AVAudioSession!
    var audioRecorder : AVAudioRecorder!
    let audioFileName = "audio-recorder.m4a"

    func directoryUrl()->URL? {
        let fileManager = FileManager.default

        let urls=fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = urls[0] as URL
        do {
            return  try documentsDirectory.appendingPathComponent(audioFileName)

        }
        catch {
            print (" en audioFileName" )
        }
        return nil

    }

    func startRecording() {
        let settings = [AVFormatIDKey : Int(kAudioFormatMPEG4AAC),
                        AVSampleRateKey: 12000.0,
                        AVNumberOfChannelsKey: 1 as NSNumber,
                        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue] as [String : Any]

        do {
            // donde va grabar el audio
            audioRecorder = try AVAudioRecorder(url: directoryUrl()!, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            // tomaremos 30 segundos de audio , luego ese audio  lo enviaremos al text to speach
            Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.stopRecording), userInfo: nil, repeats: false)

        } catch {
            print("no se ha podido grabar el audio correctamente")
        }

    }


    @objc func stopRecording(){
        audioRecorder.stop()
        audioRecorder = nil

        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reconizedSpeach), userInfo: nil, repeats: false)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // self.reconizedSpeach()
        self.recordingAudioSetup()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func recordingAudioSetup() {
        self.audioRecondingSession=AVAudioSession.sharedInstance()

        do {
            try audioRecondingSession.setCategory(AVAudioSessionCategoryRecord)

            try  audioRecondingSession.setActive(true)
            audioRecondingSession.requestRecordPermission({ [unowned self ] (allowed:Bool) in
                if allowed {
                    self.startRecording()

                }
                else {
                    print ("Fallo al configurar los permisos del microfono")
                }

            })

        }catch {
            print("Se encontro un error al configurar el audiRecorder")
        }


    }

    @objc func reconizedSpeach() {

        SFSpeechRecognizer.requestAuthorization{ (autStatus) in
            if autStatus == SFSpeechRecognizerAuthorizationStatus.authorized {

                //  let urlPath = Bundle.main.url(forResource: "audio", withExtension: "mp3")
                let reconizer=SFSpeechRecognizer()
                // vamos a llamar al

                let request = SFSpeechURLRecognitionRequest(url: self.directoryUrl()!)
                reconizer?.recognitionTask(with: request, resultHandler: { (result, error) in
                    if let error = error {
                        print("algo paso mal , :\(error.localizedDescription)")
                    }
                    else {
                        if let formattedString = result?.bestTranscription.formattedString {
                            self.richText.text=String(formattedString)
                        }

                    }

                })
            }
            else{
                print("Necesitamo permisos para acceder al audio ")
            }

        }
    }


}

